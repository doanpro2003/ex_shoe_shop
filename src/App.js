// import logo from './logo.svg';
import "./App.css";
import ExShoeShop from "./ExShoeShop/ExShoeShop";

function App() {
    return (
        <div className="App">
            <ExShoeShop />
        </div>
    );
}

export default App;
